# Keelback

**[WARNING] This language is a work in progress and anything can change at any moment without notice. It will not be uploaded until I make it self hosted...**

This language is similar too [Python](https://www.python.org/) but is statically typed and much faster. Oh it's also written in [Python](https://www.python.org/) xD

Milestones:
- [ ]  Compiled
- [ ]  Native
- [ ]  Turing-complete
- [ ]  Statically typed
- [ ]  Standard Library
- [ ]  Self-hosted
- [ ]  Optimized
- [ ]  Cross-platform
- [ ]  VS Code extensions

## Language use case

You know... it's a [thing](https://en.wikipedia.org/wiki/Programming_language).

## Examples

Hello world!

```keelback
print("Hello world!")
```

## Quick Start

### Windows

Coming Soon...

### Linux

Coming Soon...

### MAC

Coming Soon...

## Support Servers

We currently have one: [Main Server](https://discord.gg/ZDJgNeF5CS)
